package org.apache.spark.examples.ml
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._

val sc = new SparkContext("local", "Neural network", new SparkConf())

val data = spark.read.format("libsvm").load("NN_dataset.txt")

val breakup_data = data.randomSplit(Array(0.7, 0.3), seed = 1234L)

val training_data = breakup_data(0)
val test_data = breakup_data(1)

val network_structure = Array[Int](4, 6, 5, 3)

val trainer = new MultilayerPerceptronClassifier().setLayers(network_structure).setBlockSize(128).setSeed(1234L).setMaxIter(200)

val model = trainer.fit(training_data)

val result = model.transform(test_data)
val labels = result.select("prediction", "label")
val eval = new MulticlassClassificationEvaluator().setMetricName("accuracy")

println(s"Accuracy on the test dataset = ${ eval.evaluate(labels) }")

sc.stop()
System.exit(0)